\documentclass[a4paper, DIV=15]{scrartcl}

    \usepackage[utf8]{inputenc}
    \usepackage{tgcursor}
    \usepackage{tgheros}
    \usepackage{mathpazo}
    \usepackage[T1, euler-digits]{eulervm}
    \usepackage[T1]{fontenc}

    \usepackage{typearea}
    \recalctypearea{}

    \usepackage{amsmath}
    \usepackage{cancel}
    \usepackage{siunitx}

    \usepackage{float}
    \usepackage{rotating}

    \usepackage{booktabs}

    \usepackage{tikz}
    \usepackage{pgfgantt}
    \usepackage{pgfplots}
    \pgfplotsset{compat = 1.15}
    \usetikzlibrary{positioning}
    \setganttlinklabel{f-s}{}

    \setcounter{secnumdepth}{-2}

    \newcommand{\exercise}[1]{\section{Aufgabe~#1}}
    \newcommand{\exercisepart}[1]{\paragraph{#1)}}

\begin{document}
    \title{Embedded Systems Engineering \\ Übung 4}
    \author{Markus Haug}
    \date{29. Mai 2018}
    \maketitle

    \exercise{1}
    \exercisepart{a}
    Assuming \(f[x]\) is available in a register as part of the previous iteration, we have the following operations
    \[
        f[x] = \underbrace{
            \underbrace{b \cdot f[x - 1]}_{1} -
            \underbrace{
                (\underbrace{
                    5 \cdot \underbrace{(x - a)}_{2}
                }_{3} - 3)}_{4}
            }_{5}
    \]

    Scheduling these operations using RCS gives the following schedule:
    \begin{align*}
        \tau(1) &= 3 & \tau(2) &= 0 & \tau(3) &= 1 \\
        \tau(4) &= 3 & \tau(5) &= 5
    \end{align*}

    \exercisepart{b}
    High level synthesis yields the following data path and controller

    \begin{figure}[H]
        \centering
        \input{e1datapath1.tex}
        \caption{Data path for ADDSUB}
    \end{figure}

    \begin{figure}[H]
        \centering
        \input{e1datapath2.tex}
        \caption{Data path for MULT}
    \end{figure}

    \begin{sidewaystable}
        \centering
        \input{e1controller.tex}
        \caption{Controller}
    \end{sidewaystable}

    \exercisepart{c}
    If the multiplier is replaced with a pipelined version, we must reconsider the schedule.
    In the new schedule operation \(1\) can start at cycle \(0\).
    That means operation \(5\) can already start at cycle \(4\).
    Thus the overall latency of the computation decreases to \(5\) cycles.

    The data path can be kept as it is.

    We must also reconsider our controller to account for the new schedule.

    \exercise{2}
    \exercisepart{a}~

    \begin{figure}[H]
        \centering
        \input{e2a.tex}
        \caption{Gantt chart for a schedule for one iteration}
    \end{figure}

    \exercisepart{b}
    If we employ data path pipelining, we can start the a new iteration every third cycle.

    \begin{figure}[H]
        \centering
        \input{e2b.tex}
        \caption{Gantt chart for a schedule for one iteration}
    \end{figure}

    \exercisepart{c}
    We can not improve the performance using pipelined resources, since each iteration depends on the results of the
    previous one.

    \exercise{3}
    \exercisepart{a}
    The critical path consists of one multiplication (operation \(6\)) and two subtractions (operations \(8\) and 
    \(10\)).
    The multiplication takes \(4\) cycles while each subtraction takes \(2\) cycles.
    Therefore the minimum latency of the data path is \(L = 8\) cycles.

    \exercisepart{b}
    When using ASAP scheduling und multicycle resources, we need \(3\) adders, \(1\) subtractor and \(3\) multipliers.
    The total cost of this would be \(10.5\).

    An example of this ASAP schedule can be seen here:

    \begin{figure}[H]
        \centering
        \input{e3b1.tex}
        \caption{ASAP schedule for DFG}
    \end{figure}
    
    We could compress this schedule to use fewer adders.
    This could look like the following
    
    \begin{figure}[H]
        \centering
        \input{e3b2.tex}
        \caption{Modified schedule for DFG}
    \end{figure}

    \exercisepart{c}
    Data path pipelining is not possible if the results of the critical path are used as inputs to the critical path in
    the next iteration.

    Another problem that prevents data path pipelining is when a resource is utilized fully during one iteration.
    Without adding additional resources no data path pipelining is possible.
    
    To calculate throughput one must consider the latency of the data path and the length of overlap originating from
    pipelining.

    \exercisepart{d}
    Since the latency of the data path is \(8\) cycles \(n=5\) iterations of the DFG take at least \(40\) cycles when no
    data path pipelining is employed.
    
    Assuming there are no dependencies between the iterations we can pipeline the data path.
    For that we will need one additional subtractor.
    The longest operation on the critical path takes \(4\) cycles.
    So we can start a new iteration every fourth cycle (\(p = 4\)).
    This leads to a required time of
    \begin{align*}
        L + (n-1) \cdot p &= 8 \text{ cycles} + (5 - 1) \cdot 4 \text{ cycles}  \\
        &= 8 \text{ cycles} + 4 \cdot 4 \text{ cycles}  \\
        &= 8 \text{ cycles} + 16 \text{ cycles} \\
        &= 24 \text{ cycles}
    \end{align*}

    \exercisepart{e}
    A pipelined resource needs one internal register stage between each pair of subsequent operation stages.
    So for a pipelined resource with \(n\) stages, we would need \(n-1\) internal register stages.

    \exercisepart{f}
    The minimum latency of the data path doesn't change, since the critical path is the limiting factor in our case.
    The critical path can not be sped up using pipelined resources.
    Therefore the minimum latency still is \(8\) cycles.

    \exercisepart{g}
    To achieve minimum latency we can use our pipelined resources to reduce the amount of required resource instances.
    In this case we only need one instance of each resource type.
    The cost then reduces to \(4.5\).

    An example schedule could look like this:
    \begin{figure}[H]
        \centering
        \input{e3g1.tex}
        \caption{Schedule for the DFG using pipelined resources}
    \end{figure}
    
    \iffalse
    If we also pipeline the data path, an iteration could look like this:
    \begin{figure}[H]
        \centering
        \input{e3g2.tex}
        \caption{Schedule for the DFG using pipelined resource (with data path pipelining)}
    \end{figure}
    \fi
\end{document}