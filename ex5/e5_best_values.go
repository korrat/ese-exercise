package main

import (
	"fmt"
	"math"
	"math/big"
)

var (
	uP1  = big.NewRat(2, 7)
	uP2  = big.NewRat(3, 10)
	uPer = (&big.Rat{}).Add(uP1, uP2)

	schedBound = 3 * (math.Pow(2, 1.0/3.0) - 1)
)

func debug(args ...interface{}) {
	fmt.Print("\tDEBUG: ")
	fmt.Println(args...)
}

func main() {
	fmt.Println("Upper schedulability bound is", schedBound)

	best := &big.Rat{}

	var pS, cS int64
	for pS = 8; pS < 10; pS++ {
		for cS = 1; cS <= pS; cS++ {
			cur := big.NewRat(cS, pS)
			if cur.Cmp(best) < 1 {
				debug("Skipping test", cur, "since it doesn't improve our best value")
				continue
			}

			debug("Testing values", cur)
			curU := (&big.Rat{}).Add(uPer, cur)
			debug("Utilization for test is", curU)

			if f, _ := curU.Float64(); f <= schedBound {
				debug("Updating best values to", cur)
				best.Set(cur)
			} else {
				// Every following value will be even greater
				debug("Skipping further tests for period", cur.Denom(), "since a computation time of", cur.Num(),
					"already exceeds the schedulability limit")
				break
			}
		}
	}

	fmt.Println("Best values for polling server:")
	fmt.Println("c_S =", best.Num())
	fmt.Println("p_S =", best.Denom())
}
