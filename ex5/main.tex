\documentclass[a4paper, DIV=15]{scrartcl}

    \usepackage[utf8]{inputenc}
    \usepackage{tgcursor}
    \usepackage{tgheros}
    \usepackage{mathpazo}
    \usepackage[T1, euler-digits]{eulervm}
    \usepackage[T1]{fontenc}

    \usepackage{typearea}
    \recalctypearea{}

    \usepackage{amsmath}
    \usepackage{cancel}
    \usepackage{siunitx}

    \usepackage{float}
    \usepackage{rotating}

    \usepackage{booktabs}

    \usepackage{tikz}
    \usepackage{pgfgantt}
    \usepackage{pgfplots}
    \pgfplotsset{compat = 1.15}
    \usetikzlibrary{patterns, positioning}
    \setganttlinklabel{f-s}{}

    \setcounter{secnumdepth}{-2}

    \newcommand{\exercise}[1]{\section{Aufgabe~#1}}
    \newcommand{\exercisepart}[1]{\paragraph{#1)}}

\begin{document}
    \title{Embedded Systems Engineering \\ Übung 5}
    \author{Markus Haug}
    \date{29. Mai 2018}
    \maketitle

    \exercise{1}
    A frame-based cyclic executive scheduler has timer interrupts occur at every start of a frame.
    
    An activation-based cyclic executive scheduler has timer interrupts occur at each process activation.

    \exercise{2}
    \exercisepart{a}
    The constraint \(f \leq p_i\) gives allows for the frame sizes \(\lbrace 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 \rbrace\).

    The second constraint \(c_i \leq f\) limits the suitable frame sizes to \(\lbrace 2, 3, 4, 5, 6, 7, 8, 9, 10 \rbrace
    \).

    When evaluating \(2f - \gcd(f, p)\) for each combination of process and possible frame size, we can eliminate
    \(\lbrace 7, 8, 9, 10 \rbrace\). 
    This leaves \(\lbrace 2, 3, 4, 5, 6 \rbrace\) as possible frame sizes.

    I chose \(f = 2\) since it cleanly divides the period of three of the four processes.

    \exercisepart{b}
    This results in the following schedule for one major cycle

    \begin{sidewaysfigure}
        \centering
        \resizebox{0.9\textheight}{!}{\input{e2b.tex}}
    \end{sidewaysfigure}

    \exercise{3}
    \exercisepart{a}
    The process B gets priority \(1\), C gets \(2\) and A gets \(3\).

    \exercisepart{b}
    The necessary condition is
    \[
        U = \sum_{i = 1}^{n} \frac{c_i}{p_i} \leq 1
    \]
    Here it is 
    \[
        U = \sum_{i = 1}^{3} \frac{c_i}{p_i}  = \frac{1}{3} + \frac{1}{6} + \frac{4}{9} = \frac{17}{18} \leq 1
    \]
    So the necessary condition is fulfilled.
    
    The sufficient condition is 
    \[
        U \leq n(2^{\frac{1}{n}} - 1)
    \]
    Here this evaluates to 
    \[
        3 \cdot (2^{\frac{1}{3}} - 1) \geq 3 \cdot (1.25 - 1) = 0.75 \leq \frac{17}{18} \approx 0.94
    \]
    So we don't know if this problem is schedulable.

    \exercisepart{c}
    If we try to schedule the problem, we get the following
    \begin{figure}[H]
        \centering
        \input{e3c.tex}
    \end{figure}

    This proves that the problem is schedulable although the sufficient condition is not fulfilled.

    \exercise{4}
    \exercisepart{a}
    The utilization of the process set is
    \[
        U = \frac{3}{4} \leq 1
    \]
    The necessary condition for schedulability is fulfilled.
    
    The sufficient condition is
    \[
        \sum_{i = 1}{3} \frac{c_i}{d_i} \leq n(2^{\frac{1}{n}} - 1)
    \]
    If we fill in the values we get
    \[
        \frac{3}{7} + \frac{2}{4} + \frac{2}{9} = \frac{145}{126} \not\leq 0.75
    \]
    The sufficient condition is not fulfilled, so we can't be sure if the problem is schedulable.

    \exercisepart{b}
    We already determined that \(U < 1\), so the first part of the algorithm is proven.
    
    Our most important process B has a WCRT of \(w_1 = 2\).
    This fulfills the deadline.
    
    For our second most important process A we need to iterate using fixed-point iteration:
    \begin{align*}
        \text{Initial value:} \quad
        x &= \left\lceil \frac{c_2}{1 - \sum_{j = 1}^{1} \frac{c_j}{p_j}} \right\rceil
           = \left\lceil \frac{3}{1 - \frac{2}{4}} \right\rceil
           = \left\lceil \frac{3}{\frac{1}{2}} \right\rceil = 6 \\
        x &= c_2 + \sum_{j=1}^{1} c_j \cdot \left\lceil \frac{x}{p_j} \right\rceil
           = 3 + 2 \cdot \left\lceil \frac{6}{5} \right\rceil
           = 3 + 2 \cdot 2 = 7 \\
        x &= c_2 + \sum_{j=1}^{1} c_j \cdot \left\lceil \frac{x}{p_j} \right\rceil
           = 3 + 2 \cdot \left\lceil \frac{7}{5} \right\rceil
           = 3 + 2 \cdot 2 = 7 \\
    \end{align*}
    So the WCRT for process A is \(w_2 = 7\), which fulfills the deadline.
    
    For the least important process C we also need to iterate:
    \begin{align*}
        \text{Initial value:} \quad
        x &= \left\lceil \frac{c_3}{1 - \sum_{j = 1}^{2} \frac{c_j}{p_j}} \right\rceil
           = \left\lceil \frac{2}{1 - \left(\frac{2}{5} + \frac{3}{20}\right)} \right\rceil
           = \left\lceil \frac{2}{1 - \frac{11}{20}} \right\rceil
           = \left\lceil \frac{2}{\frac{9}{20}} \right\rceil = 5 \\
        x &= c_3 + \sum_{j=1}^{2} c_j \cdot \left\lceil \frac{x}{p_j} \right\rceil
           = 2 + \left(2 \cdot \left\lceil \frac{5}{5} \right\rceil \right)
               + \left(3 \cdot \left\lceil \frac{5}{20} \right\rceil \right) \\
          &= 2 + 2 \cdot 1 + 3 \cdot 1 = 7 \\
        x &= c_3 + \sum_{j=1}^{2} c_j \cdot \left\lceil \frac{x}{p_j} \right\rceil
           = 2 + \left(2 \cdot \left\lceil \frac{7}{5} \right\rceil \right)
               + \left(3 \cdot \left\lceil \frac{7}{20} \right\rceil \right) \\
          &= 2 + 2 \cdot 2 + 3 \cdot 1 = 9 \\
        x &= c_3 + \sum_{j=1}^{2} c_j \cdot \left\lceil \frac{x}{p_j} \right\rceil
           = 2 + \left(2 \cdot \left\lceil \frac{9}{5} \right\rceil \right)
               + \left(3 \cdot \left\lceil \frac{9}{20} \right\rceil \right) \\
          &= 2 + 2 \cdot 2 + 3 \cdot 1 = 9 \\
    \end{align*}
    We get a WCRT for C that is \(w_3 = 9\), which stays within the deadline.

    So the problem is schedulable using DMS.

    \exercisepart{c}
    Since the deadlines are before the periods of the task, we have to use EDF* scheduling.
    The sufficient condition for schedulability in this case is:
    \[
        \forall \min_{i = 1, \ldots, n} \lbrace d_i \rbrace \leq t \leq t^*:
        CCt) = \sum_{i = 1}^{n} \left( \left\lfloor \frac{t - d_i}{p_i} \right\rfloor + 1 \right) \cdot c_i
        \leq t
    \]
    The lower limit is \(\min{d_i} = 4\).
    The upper limit \(t^*\) can be calculated as
    \begin{align*}
        t^* &= \frac{U}{1 - U} \cdot \max_{i = 1, \ldots, n} \lbrace p_i - d_i \rbrace \\
            &= \frac{\frac{3}{4}}{1 - \frac{3}{4}} \cdot \max_{i = 1, 2, 3} \lbrace p_i - d_i \rbrace \\
            &= \frac{\frac{3}{4}}{\frac{1}{4}} \cdot \max \lbrace 5 - 4, 20 - 7, 10 - 9 \rbrace \\
            &= 3 \cdot \max \lbrace 1, 13, 1 \rbrace \\
            &= 3 \cdot 13 = 39
    \end{align*}

    If we check \(C(t) \leq t\) for all time steps \(t\) with \(4 \leq t \leq 39\) we see, that the process set is
    schedulable using EDF* scheduling.

    To do this we can use a small program that, given a set of processes, computes the lower and upper bounds for \(t\)
    and checks \(C(t) \leq t\) for each time step between the bounds.

    \exercise{5}
    First we compute an upper limit to guarantee schedulability:
    \[
        (n + 1) \left(2^{\frac{1}{n + 1}} - 1 \right) 
        = (2 + 1) \left(2^{\frac{1}{2 + 1}} - 1 \right)
        = 3 \cdot \left(2^{\frac{1}{3}} - 1 \right)
        \geq 3 \cdot \left(1.25 - 1 \right)
        = 3 \cdot 0.25 = 0.75 = \frac{3}{4}
    \]
    Then we can compute the utilization for the periodic tasks:
    \[
        U = \frac{c_1}{p_1} + \frac{c_2}{p_2}
          = \frac{2}{7} + \frac{3}{10}
          = \frac{20}{70} + \frac{21}{70} = \frac{41}{70}
    \]
    We can then compute the available computation time and period for the polling server:
    \[
        \frac{c_S}{p_S} \leq \frac{3}{4} - U 
        = \frac{3}{4} - \frac{41}{70} 
        = \frac{105}{140} - \frac{82}{140} = \frac{23}{140}
    \]
    Since we want our polling server to have medium priority, we want \(p_S\) to be \(7 < p_S < 10\).

    I chose \(p_S = 8\) and \(c_S = 1\).
    This means
    \[
        \frac{c_S}{p_S} = \frac{1}{8} = \frac{20}{160} \leq \frac{23}{140}
    \]
    Therefore our problem is still schedulable.

    The hypercycle here is \(280\).
    
    We can then come up with the following schedule (only showing the first 20 cycles)
    \begin{sidewaysfigure}
        \centering
        \resizebox{0.9\textheight}{!}{\input{e5.tex}}
    \end{sidewaysfigure}

    \exercise{6}
    Our periodic processes create a CPU utilization of
    \[
        U_P = \frac{c_1}{p_1} + \frac{c_2}{p_2}
            = \frac{2}{6} + \frac{3}{9} = \frac{2}{3}
    \]
    This leaves us with a remaining utilization of \(U_S = \frac{1}{3}\) for scheduling aperiodic tasks.

    From this we can compute the deadlines for our aperiodic tasks with the following formula:
    \[
        d_k = \max \lbrace d_{k - 1}, a_k \rbrace + \frac{c_k}{U_S}
            = \max \lbrace d_{k - 1}, a_k \rbrace + c_k \cdot 3
    \]
    If we actually compute the deadlines, we get the following table:
    \begin{table}[H]
        \centering
        \begin{tabular}{*{4}{>{\(} r <{\)}}}
            \toprule
                & J_1 & J_2 & J_3 \\ \midrule
            a_k &  1  &  5  & 15  \\
            c_k &  3  &  1  &  1  \\
            d_k & 10  & 13  & 18  \\ \bottomrule
        \end{tabular}
    \end{table}

    From this we can derive a schedule
    \begin{sidewaysfigure}
        \centering
        \resizebox{0.9\textheight}{!}{\input{e6.tex}}
    \end{sidewaysfigure}
\end{document}