# --- Parameters: defaults can be overwritten with .dat file

set Operations    ordered;
set Resources     ordered;
set RunOn within {Operations, Resources};

param Delay {Operations} integer >= 0 default 1;
param Before {Operations, Operations} binary default 0;

param MaxLatency integer >= 0;

param Cost {Resources} integer >= 0 default 1;
param Pipelined {Resources} binary default 0;

# --- Variable: Tscheduled[op] is time at which op starts

var Tscheduled {op in Operations} integer >= 0;
var Tstart {op in Operations, t in 0 .. MaxLatency} binary;

var Allocation {Resources} integer >= 0;

# --- Optimization goal

minimize TotalCost: 
    sum {r in Resources} Cost[r] * Allocation[r];

# No good since nonlinear:   
# max {op in Operations} (Tscheduled[op] + Delay[op]);
# use constraint ComputeLatency instead!

# --- Constraints

subject to LatencyConstraint {op in Operations}:
   MaxLatency >= Tscheduled[op] + Delay[op];

subject to Feasibility {o1 in Operations, o2 in Operations
                        : Before[o1,o2] }:
   Tscheduled[o1] + Delay[o1] <= Tscheduled[o2];

subject to ConnectScheduleAndStart {op in Operations}:
    sum {t in 0 .. MaxLatency} t * Tstart[op, t] = Tscheduled[op];

subject to ResourceConstraints {r in Resources, t in 0 .. MaxLatency : !Pipelined[r]}:
    (sum {v in Operations : 
        (v, r) in RunOn} sum {p in 0 .. Delay[v] - 1} Tstart[v, t - p]) <= Allocation[r];

subject to PipelinedResourceConstraints {r in Resources, t in 0 .. MaxLatency : Pipelined[r]}:
    (sum {v in Operations : 
        (v, r) in RunOn} sum {p in 0 .. Delay[v] - 1} Tstart[v, t - p]) <= Allocation[r];

subject to PlanTaskOnce {op in Operations}:
    sum {t in 0 .. MaxLatency} Tstart[op, t] = 1;