# --- Parameters: defaults can be overwritten with .dat file

set Operations    ordered;
set Resources;
set RunOn within {Operations, Resources};

param Delay {Operations} integer >= 0 default 1;
param Before {Operations, Operations} binary default 0;

param T integer = sum {v in Operations} Delay[v];

param Allocation {Resources} integer >= 0;
param Pipelined {Resources} binary default 0;

# --- Variable: Tscheduled[op] is time at which op starts

var Tscheduled {op in Operations} integer >= 0;

var MaxLatency integer >= 0;

var Tstart {op in Operations, t in 0 .. T} binary;

# --- Optimization goal

minimize Latency: MaxLatency;

# --- Constraints

subject to ComputeLatency {op in Operations}:
   MaxLatency >= Tscheduled[op] + Delay[op];

subject to Feasibility {o1 in Operations, o2 in Operations
                        : Before[o1,o2] }:
   Tscheduled[o1] + Delay[o1] <= Tscheduled[o2];

subject to ConnectScheduleAndStart {op in Operations}:
    sum {t in 0 .. T} t * Tstart[op, t] = Tscheduled[op];

subject to ResourceConstraints {r in Resources, t in 0 .. T : !Pipelined[r]}:
    (sum {v in Operations : (v, r) in RunOn}
        sum {p in 0 .. Delay[v] - 1 : t - p >= 0} Tstart[v, t - p]) <= Allocation[r];

subject to PipelinedResourceConstraints {r in Resources, t in 0 .. T : Pipelined[r]}:
    (sum {v in Operations : (v, r) in RunOn} Tstart[v, t]) <= Allocation[r];

subject to PlanTaskOnce {op in Operations}:
    sum {t in 0 .. T} Tstart[op, t] = 1;